package services.mqtt

import java.net._

import javax.cache.Cache
import main.CacheHandler
import org.apache.activemq.broker.{BrokerService, TransportConnector}
import org.apache.activemq.network.NetworkConnector
import org.apache.ignite.cache.query.{QueryCursor, ScanQuery}
import org.apache.ignite.lang.IgniteBiPredicate
import org.apache.ignite.resources.IgniteInstanceResource
import org.apache.ignite.services.{Service, ServiceContext}
import org.apache.ignite.{Ignite, IgniteCache}
import services.network.NetworkCourier
import utils.Log

import scala.collection.JavaConverters._

class BrokerContainerServiceImpl(val instanceID: String /*, val portOffset: Int*/) extends BrokerContainerService with Service {

  private val MQTT_BASE_SCOPE = "mqtt://0.0.0.0:" //FIXME
  private val TCP_BASE_SCOPE = "tcp://0.0.0.0:"

  private var mqttPort: Int = _
  private var tcpPort: Int = _


  private var address: String = _
  private var mqttSocket: String = _
  private var tcpSocket: String = _
  private var info: BrokerInfo = _

  private def TAG = {
    /*instanceID + ", " +*/ mqttPort + ", " + tcpPort
  }

  /** Auto-injected instance of Ignite. */
  @IgniteInstanceResource var ignite: Ignite = _


  /** Distributed cache used to store service names. */
  private var cache: IgniteCache[Any, Any] = _

  /** Distributed cache used to store other brokers reference */
  private var brokerCache: IgniteCache[String, BrokerInfo] = _

  /** Service name. */
  private var svcName = ""

  private var broker: BrokerService = _


  override def cancel(ctx: ServiceContext): Unit = {
    // Remove service from cache.
    cache.remove(svcName)
    Log.d(TAG, "Cancel ")
  }

  override def init(ctx: ServiceContext): Unit = {
    val portCourier: NetworkCourier = ignite.services.serviceProxy("PortCourier", classOf[NetworkCourier], false)

    mqttPort = portCourier.claimMqttPort
    tcpPort = portCourier.claimTcpPort

    address = findAddress()
    mqttSocket = makeSocket(address, mqttPort)
    tcpSocket = makeSocket(address, tcpPort)
    cache = ignite.cache(CacheHandler.DEFAULT_CACHE_NAME)
    brokerCache = ignite.cache(CacheHandler.DEFAULT_BROKER_CACHE)
    info = BrokerInfo(address, mqttPort, tcpPort)
    brokerCache.put(instanceID, info)

    svcName = ctx.name
    Log.d(TAG, "Service was initialized: " + svcName + " on node: " + ignite.name())

    broker = new BrokerService()

    broker.setBrokerName("MQTT_broker_" + instanceID)
    broker.setPersistent(false)
    broker.setUseJmx(false)

    val mqttConnector = new TransportConnector //Used for mqtt communications
    mqttConnector.setName("mqtt")
    mqttConnector.setUri(new URI(MQTT_BASE_SCOPE + mqttPort))
    mqttConnector.setAllowLinkStealing(true)
    //connector.setDiscoveryUri(new URI("multicast://default"))
    broker.addConnector(mqttConnector)

    val internalConnector = new TransportConnector //Used for clustering
    internalConnector.setName("tcp")
    internalConnector.setUri(new URI(TCP_BASE_SCOPE + tcpPort))
    internalConnector.setAllowLinkStealing(false)
    broker.addConnector(internalConnector)

    Log.d(TAG, "Node: " + ignite.cluster().localNode().id() + ", Deploying brokerService on port " + mqttPort)

  }

  override def execute(ctx: ServiceContext): Unit = {
    Log.d(TAG, "Execute ")
    broker.start()
  }


  def makeSocket(ip: String, port: Int): String = {
    ip + ":" + port
  }

  def findAddress(): String = { //TODO: optimize
    var addr: String = ""
    var ip: String = ""
    try {
      val interfaces = NetworkInterface.getNetworkInterfaces
      while (interfaces.hasMoreElements) {
        val iface = interfaces.nextElement
        // filters out 127.0.0.1 and inactive interfaces

        if (!iface.isLoopback && iface.isUp) {
          val addresses = iface.getInetAddresses
          while (addresses.hasMoreElements) {
            val addr = addresses.nextElement
            // *EDIT*
            if (addr.isInstanceOf[Inet4Address]) {
              ip = addr.getHostAddress
            }
          }
        }
      }
    }
    catch {
      case e: SocketException => throw new RuntimeException(e)
    }
    ip
  }


  override def discoverNeighborhood: Unit = {

    val query: ScanQuery[String, BrokerInfo] = new ScanQuery[String, BrokerInfo](
      new IgniteBiPredicate[String, BrokerInfo]() {
        override def apply(e1: String, e2: BrokerInfo): Boolean = {
          !e1.equals(instanceID)
        }
      }


    )
    val cursor: QueryCursor[Cache.Entry[String, BrokerInfo]] = brokerCache.query(query)

    cursor.asScala.foreach { entry =>
      connectTo(entry.getValue)
    }
  }


  override def connectTo(remote: BrokerInfo): Unit = {
    Log.d(TAG, s"Creating connection ${connectionName(remote)}")
    val n: NetworkConnector = broker.addNetworkConnector("static:(" + remote.tcpConnectionString + ")")
    n.setName(connectionName(remote))
    //Log.d(instanceID, "Creating network connector: " + n.getName)
    n.setDuplex(false)
    //n.setNetworkTTL(1)
    n.start()

    /*
    if (shouldConnectWith(remote)) { //Avoid double and self connections
      //Log.d(TAG, s"connecting to: " + remote)
      val n: NetworkConnector = broker.addNetworkConnector("static:(" + remote.tcpConnectionString + ")")
      n.setName(connectionName(info, remote))
      //Log.d(instanceID, "Creating network connector: " + n.getName)
      n.setDuplex(true)
      n.setNetworkTTL(1)
      n.start()
    }
    */
  }

  private def shouldConnectWith(remote: BrokerInfo): Boolean = {
    Log.d(TAG, s"Checking possible connection to $remote")
    if (remote.address == address && remote.tcpPort == tcpPort) {
      Log.wtf(TAG, s"Trying to self connect: $remote. Discarded")
      return false
    } // Avoid connect to self (redundant. TODO: check and remove

    if (broker.getNetworkConnectors.size() > 0) { //Debug.
      broker.getNetworkConnectors.asScala.foreach { entry => {
        Log.d(TAG, s" has connection ${entry.getName}")
        if (entry.getName.equals(connectionName(remote, info))) { //If the connection was made by the other party the names are interchanged
          Log.d(TAG, s"Trying to double connect to $remote. Discarded")
          return false
        }
      }
      }
    }
    Log.d(TAG, s"Connection with $remote accepted")
    true


    /*
    if(tcpPort < remote.tcpPort) {
      /** NOTE to self:
        In a case in which all broker where instantiated before this point, it should not be possible to connect to lower
        port numbers to avoid double connections (which are full duplex). In a dynamic case in which brokers are
        instantiated after other ones passed this stage, it should be required to force also connections to upper port numbers
      */

      if(broker.getNetworkConnectors.size() > 0){ //Debug. FIXME: remove this
        broker.getNetworkConnectors.asScala.foreach{entry => {
          if(entry.getName.contains(remote.tcpPort)){
            Log.d(TAG, s"Trying to double connect to $remote. Discarded")
            return false
          }
        }}
      }
      Log.d(TAG, s"Non synchronized broker found. Connecting to $remote")
      return true
    }
    true
    */
  }

  /**
    * Convenience method to create connection names
    *
    * @param local
    * @param remote
    * @return
    */
  private def connectionName(local: BrokerInfo, remote: BrokerInfo): String = {
    s"${local.address}:${local.tcpPort} to ${remote.address}:${remote.tcpPort}"
  }

  private def connectionName(remote: BrokerInfo): String = {
    s"${info.address}:${info.tcpPort} to ${remote.address}:${remote.tcpPort}"
  }

  override def getMqttSocket: String = mqttSocket

  override def getTcpSocket: String = tcpSocket
}


trait BrokerContainerService {

  def getMqttSocket: String

  def getTcpSocket: String

  def discoverNeighborhood: Unit

  def connectTo(remote: BrokerInfo): Unit
}
