package services.mqtt

import java.util

import org.apache.ignite.resources.IgniteInstanceResource
import org.apache.ignite.{Ignite, IgniteCache, IgniteDataStreamer}
import org.apache.ignite.services.{Service, ServiceContext}
import org.apache.ignite.internal.IgnitionEx.grid
import org.apache.ignite.stream.mqtt.MqttStreamer
import com.google.common.base.Splitter
import main.CacheHandler
import org.apache.ignite.internal.util.lang.GridMapEntry
import org.eclipse.paho.client.mqttv3.MqttMessage


class StreamerServiceImpl(val brokerUrl: String, val topics: util.List[String]) extends StreamerService with Service {

  /** Auto-injected instance of Ignite. */
  @IgniteInstanceResource var ignite: Ignite = _

  /** Distributed cache used to store counters. */
  private var cache: IgniteCache[Any, Any] = _

  /** Service name. */
  private var svcName = ""

  private var streamer: MqttStreamer[Integer, String] = _


  override def cancel(ctx: ServiceContext): Unit = {
    // Remove service from cache.
    cache.remove(svcName)
    println("Cancel")
  }

  override def init(ctx: ServiceContext): Unit = {

    cache = ignite.cache(CacheHandler.DEFAULT_CACHE_NAME)
    svcName = ctx.name
    println("Service was initialized: " + svcName + " on node: " + ignite.name())

    val dataStreamer: IgniteDataStreamer[Integer, String] = grid(ignite.name()).dataStreamer("MyCache")


    // Create an MQTT data streamer
    streamer = new MqttStreamer[Integer, String]
    streamer.setIgnite(ignite)
    streamer.setStreamer(dataStreamer)
    streamer.setBrokerUrl(brokerUrl)
    streamer.setBlockUntilConnected(true)


    streamer.setSingleTupleExtractor((msg: MqttMessage) => {
      val s = Splitter.on(",").splitToList(new String(msg.getPayload))
      println("key: " + s.get(0) + ", val: " + s.get(1))
      new GridMapEntry[Integer, String](s.get(0).toInt, s.get(1))
    })


    streamer.setTopics(topics)

  }

  override def execute(ctx: ServiceContext): Unit = {
    println("Execute")
    streamer.start()

  }

  override def send(topic: String, msg: String): Unit = ???
}

trait StreamerService {

  def send(topic: String, msg: String)
}
