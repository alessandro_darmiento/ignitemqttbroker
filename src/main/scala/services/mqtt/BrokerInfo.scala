package services.mqtt

case class BrokerInfo(address: String, mqttPort: Int, tcpPort: Int) {

  override def toString: String = {
    s"ip: $address, mqtt port: $mqttPort, tcp port: $tcpPort"
  }

  def tcpConnectionString: String = {
    s"tcp://$address:$tcpPort"
  }
}
