package services.network

import main.CacheHandler
import org.apache.ignite.{Ignite, IgniteCache}
import scala.util.Random
import java.io.IOException
import java.net.ServerSocket


case class PortArray(size: Int, base: Int, ignite: Ignite, name: String) {

  if (size <= 0 || base <= 0) throw new IllegalArgumentException("suca") //TODO

  var ports = new Array[Int](size)
  var usedSize = 0
  val random = new Random()

  private val cache: IgniteCache[Int, Any] = CacheHandler.instantiateCache(ignite, name)
  private val lock = cache.lock(0)

  for (i <- 0 until size) {
    ports(i) = base + i
  }

  /**
    * Occupy a free port and put it in the cache of ports in use
    * TODO: add the reference to the caller obj in the cache
    *
    * @return
    */
  def claimFreePort(): Int = {

    lock.lock()
    if (usedSize == size) throw new ArrayIndexOutOfBoundsException("suca") //TODO
    var x: Int = 0 //assign always the first port to the first requesting actor

    do {
      x = random.nextInt(size - usedSize)
    } while (isPortInUse(ports(x)))

    val p = ports(x)
    swapOut(x)
    usedSize += 1
    cache.put(p, "TODO") //TODO
    lock.unlock()
    p
  }

  /**
    * Put back a port in the free list
    *
    * @param p
    */
  def releasePort(p: Int): Unit = {
    //TODO
    if (!cache.containsKey(p)) throw new IllegalArgumentException("suca") //TODO
    swapIn(p)
    usedSize -= 1
  }


  /**
    * @return a port used by a broker to connect with it
    */
  def getUsedPort(): Int = { //TODO
    //TODO
    base
  }

  def isUsed(port: Int): Boolean = {
    cache.containsKey(port)
  }

  def getUserSize: Int = usedSize

  private def swapOut(x: Int): Unit = {
    ports(size - usedSize - 1) = ports(x)
  }

  private def swapIn(p: Int): Unit = {
    ports(size - usedSize) = p
  }


  import java.net.Socket

  private def isPortInUse(hostName: String, portNumber: Int) = try {
    new Socket(hostName, portNumber).close()
    true
  } catch {
    case e: Exception =>
      // remote port is closed, nothing is running on
      false
  }


  private def isPortInUse(port: Int) = try {
    new ServerSocket(port).close()
    false
  } catch {
    case e: IOException =>
      true
  }

}