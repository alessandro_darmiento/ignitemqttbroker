package services.network

import org.apache.ignite.{Ignite, IgniteCache}
import org.apache.ignite.resources.IgniteInstanceResource
import org.apache.ignite.services.{Service, ServiceContext}
import utils.Log

/**
  * Node Singleton which deliver and keep track of free and used network resources
  */
class NetworkCourierImpl extends NetworkCourier with Service {
  val TAG = "services.network.networkCourier"
  val N_PORTS = 64
  val MQTT_BASE_PORT = 1883
  val TCP_BASE_PORT = 61616

  var mqttPorts: PortArray = _
  var tcpPorts: PortArray = _

  /** Auto-injected instance of Ignite. */
  @IgniteInstanceResource var ignite: Ignite = _

  /** Distributed cache used to store service names. */
  private var cache: IgniteCache[Any, Any] = _

  /** Service name. */
  private var svcName = ""

  override def cancel(ctx: ServiceContext): Unit = {
    // Remove service from cache.
    cache.remove(svcName)
    Log.d(TAG, "Cancel")
  }


  override def init(ctx: ServiceContext): Unit = {
    mqttPorts = PortArray(N_PORTS, MQTT_BASE_PORT, ignite, "mqtt")
    tcpPorts = PortArray(N_PORTS, TCP_BASE_PORT, ignite, "tcp")
  }

  override def execute(ctx: ServiceContext): Unit = {
    Log.d(TAG, "Execute")
  }

  override def claimTcpPort: Int = tcpPorts.claimFreePort()

  override def claimMqttPort: Int = mqttPorts.claimFreePort()

  override def getMqttBroker: String = {
    //TODO: get from a cache the broker address
    "tcp://localhost:" + mqttPorts.getUsedPort()
  }

}

trait NetworkCourier {

  def claimTcpPort: Int

  def claimMqttPort: Int

  def getMqttBroker: String

}
