package utils

object Log {

  def d(TAG: String, msg: String): Unit = {
    println(TAG + ": " + msg)
  }

  def wtf(TAG: String, msg: String): Unit = {
    System.err.println("WTF! " + TAG + ": " + msg)
  }

  def wtf(TAG: String, e: Exception): Unit = {
    System.err.println("WTF! " + TAG + ": " + e.getMessage + "\n" + e.getStackTrace)
  }

}
