package main;

import org.apache.ignite.*;
import org.apache.ignite.cluster.ClusterGroup;
import org.apache.ignite.cluster.ClusterNode;
import org.apache.ignite.configuration.IgniteConfiguration;
import services.mqtt.BrokerContainerService;
import services.mqtt.BrokerContainerServiceImpl;
import services.mqtt.BrokerInfo;
import services.mqtt.StreamerServiceImpl;
import services.network.NetworkCourierImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Main {

    private final static int N_NODES = 10;

    private static HashMap<String, String> attrs = new HashMap<>();


    public static void main(String args[]) {



        Ignite ignite = null;
        IgniteConfiguration cfg = new IgniteConfiguration();

        for (int i = 0; i < N_NODES; i++) {
            cfg.setIgniteInstanceName("ignite_" + i);
            attrs.put("port_offset", String.valueOf(i)); //TODO: nodes should be able to find out first free port by themselves
            if (i % 2 == 0) {
                attrs.put("ROLE", "broker");
                cfg.setUserAttributes(attrs);
            } else {
                attrs.put("ROLE", "client");
                cfg.setUserAttributes(attrs);
            }
            ignite = Ignition.start(cfg);
        }
        //Start a node to deploy port courier service
        cfg = new IgniteConfiguration();
        cfg.setIgniteInstanceName("ignite_port_courier");
        attrs.put("ROLE", "port_courier");
        cfg.setUserAttributes(attrs);
        ignite = Ignition.start(cfg);

        IgniteCache<Integer, String> cache = CacheHandler.instantiateCache(ignite);

        IgniteCluster cluster = ignite.cluster();


        //DEBUG: undeploy every service before start
        IgniteServices services = ignite.services();
        services.cancelAll();

        IgniteCache<String, BrokerInfo> brokerCache = CacheHandler.instantiateCache(ignite, CacheHandler.DEFAULT_BROKER_CACHE);
        //DEBUG: clear caches from previous run stuff
        brokerCache.clear();

        //DEBUG: differentiate groups
        ClusterGroup brokerGroup = cluster.forAttribute("ROLE", "broker");
        ClusterGroup clientGroup = cluster.forAttribute("ROLE", "client");
        ClusterGroup portCourier = cluster.forAttribute("ROLE", "port_courier");

        //Broadcast specific commands to different group
        final Ignite ign = ignite;

        IgniteCompute computePC = ignite.compute(portCourier);
        computePC.broadcast(() -> deployPortCourier(ign, new Callback() {

            @Override
            public void ready() {

                IgniteCompute computeB = ign.compute(brokerGroup); //TODO: run this only after the port courier
                computeB.broadcast(() -> deployBroker(ign));

                IgniteCompute computeC = ign.compute(clientGroup);
                //computeC.broadcast(() -> deployClient(ign));
            }
        }));

    }

    private static void printStuff(Ignite ignite) {
        System.out.println("Node: " + ignite.cluster().localNode().id() + ", role: " + ignite.cluster().localNode().attributes().get("ROLE"));
    }

    /**
     * Scope: local node
     * Deploy an MQTT broker service on the local node.
     */
    private static void deployBroker(Ignite ignite) {
        final ClusterNode local = ignite.cluster().localNode();
        final ClusterGroup localGroup = ignite.cluster().forLocal();
        final IgniteServices services = ignite.services(localGroup);

        final String id = local.id().toString();
        final int portOffset = Integer.valueOf(local.attributes().get("port_offset").toString());

        services.deployNodeSingletonAsync("BrokerService_" + id, new BrokerContainerServiceImpl(id)).listen(voidIgniteFuture -> {

            System.out.println("id: " + id + ", " + voidIgniteFuture);

            BrokerContainerService brokerService = services.
                    serviceProxy("BrokerService_" + id, BrokerContainerService.class, false);

            System.out.println("Broker mqtt socket: " + brokerService.getMqttSocket());
            System.out.println("Broker tcp socket: " + brokerService.getTcpSocket());

            brokerService.discoverNeighborhood();

        });
    }


    private static void deployClient(Ignite ignite) {
        final ClusterNode local = ignite.cluster().localNode();
        final ClusterGroup localGroup = ignite.cluster().forLocal();
        final IgniteServices services = ignite.services(localGroup);

        final List<String> topics = new ArrayList<String>() {{
            add("pippo");
            add("pluto");
            add("paperino");
            add("#");
        }};

        //IgniteDataStreamer<Integer, String> dataStreamer = grid().dataStreamer("MyCache");

        final String id = local.id().toString();
        final String role = local.attributes().get("ROLE").toString();
        System.out.println("Node: " + id + ", role: " + role + ", Deploying client service ");

        //FIXME: make it functional with internal broker
        services.deployNodeSingletonAsync("ClientService_" + id, new StreamerServiceImpl("tcp://broker.hivemq.com:1883", topics)).listen(voidIgniteFuture -> {

            System.out.println("id: " + id + ", " + voidIgniteFuture);

            BrokerContainerService brokerService = services.
                    serviceProxy("BrokerService_" + id, BrokerContainerService.class, false);
        });

    }

    private static void loadBalance(Ignite ignite) {
        System.out.println("Load balancer node is active");

        IgniteCache cache;

    }


    private static void deployPortCourier(Ignite ignite, Callback callback) {
        final ClusterGroup localGroup = ignite.cluster().forLocal();
        final IgniteServices services = ignite.services(localGroup);

        services.deployNodeSingletonAsync("PortCourier", new NetworkCourierImpl()).listen(voidIgniteFuture -> callback.ready());
    }

}



