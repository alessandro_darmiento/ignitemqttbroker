package main;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.configuration.CacheConfiguration;

import java.util.ArrayList;
import java.util.List;

import static org.apache.ignite.cache.CacheAtomicityMode.TRANSACTIONAL;

public class CacheHandler {

    public static final String DEFAULT_CACHE_NAME = "default_cache";
    public static final String DEFAULT_BROKER_CACHE = "broker_cache";

    /*
        public static IgniteCache<Integer, String> instantiateCache(Ignite ignite, String cacheName) {
            CacheConfiguration cfg = new CacheConfiguration<String, String>();

            cfg.setName(cacheName);
            cfg.setAtomicityMode(TRANSACTIONAL);

            // Create cache with given name, if it does not exist.
            return ignite.getOrCreateCache(cfg);


        }

        public static IgniteCache<Integer, String> instantiateCache(Ignite ignite) {
            return instantiateCache(ignite, "MyCache");
        }
    */
    public static <K, V> IgniteCache<K, V> instantiateCache(Ignite ignite, String cacheName) {
        CacheConfiguration cfg = new CacheConfiguration<K, V>();

        cfg.setName(cacheName);
        cfg.setAtomicityMode(TRANSACTIONAL);

        // Create cache with given name, if it does not exist.
        return ignite.getOrCreateCache(cfg);
    }

    public static <K, V> IgniteCache<K, V> instantiateCache(Ignite ignite) {
        return instantiateCache(ignite, DEFAULT_CACHE_NAME);
    }


}
